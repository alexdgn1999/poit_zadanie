from flask import Flask, render_template, jsonify
from flask_socketio import SocketIO
import serial
import threading
import time
import json
import os
import mysql.connector
from datetime import datetime

app = Flask(__name__)
socketio = SocketIO(app)

arduino_port = '/dev/cu.usbmodem11301'
arduino_baudrate = 9600
arduino_timeout = 1
arduino = None
data_list = []

monitoring = False

json_file_path = 'data.json'
if not os.path.exists(json_file_path):
    with open(json_file_path, 'w') as f:
        json.dump([], f)

def save_data_to_json(file_path, data):
    print("DATA saved to JSON");
    with open(file_path, 'r+') as file:
        try:
            existing_data = json.load(file)
        except json.JSONDecodeError:
            existing_data = []
        existing_data.extend(data)
        file.seek(0)
        json.dump(existing_data, file, indent=4)
        file.truncate()
    
def read_from_arduino():
    global arduino, monitoring, data_list
    json_file_path = 'data.json'
    while arduino:
        if monitoring and arduino.in_waiting > 0:
            line = arduino.readline().decode('utf-8').rstrip()
            if line:
                print(f"Raw data from Arduino: {line}")
                try:
                    data = json.loads(line)
                    photoSensor = data.get("photoSensor")
                    distance = data.get("distance")
                    if photoSensor is not None and distance is not None:
                        timestamp = datetime.now().isoformat()
                        data_with_timestamp = {
                            "photoSensor": photoSensor,
                            "distance": distance,
                            "timestamp": timestamp
                        }
                        socketio.emit('sensor_data', data_with_timestamp)
                        data_list.append(data_with_timestamp)
                        if len(data_list) >= 20:
                            save_data_to_json(json_file_path, data_list)
                            save_data_to_mysql(data_list)
                            data_list = []
                        print(f"Emitted data: {data_with_timestamp}")
                except json.JSONDecodeError as e:
                    print(f"Error decoding JSON data: {e}")
                except ValueError as e:
                    print(f"Error reading data: {e}")
        socketio.sleep(0.420)


def connect_to_arduino(port, baudrate, timeout):
    global arduino
    while True:
        try:
            arduino = serial.Serial(port, baudrate, timeout=timeout)
            print(f"Connected to Arduino on port {port}")
            socketio.emit('arduino_status', {'message': f'Connected to Arduino on port {port}'})
            return arduino
        except serial.SerialException as e:
            if 'Resource busy' in str(e):
                print(f"Port {port} is busy. Retrying in 2 seconds...")
                time.sleep(2)
            else:
                print(f"Failed to connect to Arduino: {e}")
                time.sleep(2)

def disconnect_from_arduino():
    global arduino
    if arduino:
        arduino.close()
        arduino = None
        print("Disconnected from Arduino")
        socketio.emit('arduino_status', {'message': 'Disconnected from Arduino'})

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/open', methods=['POST'])
def open_system():
    try:
        connect_to_arduino(arduino_port, arduino_baudrate, arduino_timeout)
        threading.Thread(target=read_from_arduino).start()
        return jsonify({"status": "success", "message": "System initialized"}), 200
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500

@app.route('/start_monitoring_distance', methods=['POST'])
def start_monitoring_distance():
    global monitoring
    monitoring = True
    if arduino:
        arduino.write(b'START_MONITORING_DISTANCE\n')
    print("Monitoring started for Distance") 
    socketio.emit('arduino_status', {'message': 'Monitoring started for Distance'})
    return jsonify({"status": "success", "message": "Monitoring started"}), 200

@app.route('/stop_monitoring_distance', methods=['POST'])
def stop_monitorin_distance():
    if arduino:
        arduino.write(b'STOP_MONITORING_DISTANCE\n')
    print("Monitoring stopped for Distance") 
    socketio.emit('arduino_status', {'message': 'Monitoring stopped for Distance'})
    return jsonify({"status": "success", "message": "Monitoring stopped"}), 200

@app.route('/start_monitoring_lumin', methods=['POST'])
def start_monitoring_lumin():
    global monitoring
    monitoring = True
    if arduino:
        arduino.write(b'START_MONITORING_LIGHT\n')
    socketio.emit('arduino_status', {'message': 'Monitoring started for Light Intensity'})
    return jsonify({"status": "success", "message": "Monitoring started"}), 200

@app.route('/stop_monitoring_lumin', methods=['POST'])
def stop_monitoring_lumin():
    if arduino:
        arduino.write(b'STOP_MONITORING_LIGHT\n')
    socketio.emit('arduino_status', {'message': 'Monitoring stopped for Light Intensity'})
    return jsonify({"status": "success", "message": "Monitoring stopped"}), 200

@app.route('/stop_monitoring_all', methods=['POST'])
def stop_monitoring_all():
    global monitoring
    monitoring = False
    if arduino:
        arduino.write(b'STOP_ALL\n')
    print("Monitoring stopped for all") 
    socketio.emit('arduino_status', {'message': 'Monitoring stopped for all'})
    return jsonify({"status": "success", "message": "Monitoring stopped"}), 200

@app.route('/disconnect', methods=['POST'])
def disconnect():
    try:
        disconnect_from_arduino()
        return jsonify({"status": "success", "message": "Disconnected from Arduino"}), 200
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500

@app.route('/get_data', methods=['GET'])
def get_data():
    try:
        with open(json_file_path, 'r') as f:
            data = json.load(f)
        return jsonify(data), 200
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500
    
@app.route('/get_data_db_distance', methods=['GET'])
def get_data_db_distance():
    try:
        db = mysql.connector.connect(
            host="localhost",
            user="root",
            password="slovakia",
            database="sensor_data"
        )
        cursor = db.cursor(dictionary=True)
        cursor.execute("SELECT * FROM measurements")
        data = cursor.fetchall()
        cursor.close()
        db.close()
        return jsonify(data), 200
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500

@app.route('/get_data_db_lumin', methods=['GET'])
def get_data_db_lumin():
    try:
        db = mysql.connector.connect(
            host="localhost",
            user="root",
            password="slovakia",
            database="sensor_data"
        )
        cursor = db.cursor(dictionary=True)
        cursor.execute("SELECT * FROM measurements")
        data = cursor.fetchall()
        cursor.close()
        db.close()
        return jsonify(data), 200
    except Exception as e:
        return jsonify({"status": "error", "message": str(e)}), 500


def save_data_to_mysql(data):
    db = mysql.connector.connect(
        host="localhost",
        user="root",
        password="slovakia",
        database="sensor_data"
    )
    cursor = db.cursor()

    for entry in data:
        sql = "INSERT INTO measurements (photoSensor, distance, timestamp) VALUES (%s, %s, %s)"
        val = (entry["photoSensor"], entry["distance"], entry["timestamp"])
        cursor.execute(sql, val)

    db.commit()
    cursor.close()
    db.close()


if __name__ == '__main__':
    socketio.run(app, debug=True)
