#include <ArduinoJson.h>

const int photoSensorPin = A0; 
const int trigPin = 11; 
const int echoPin = 12; 

bool monitoringDistance = false;
bool monitoringLight = false;

void setup() {
  Serial.begin(9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void loop() {
  if (Serial.available() > 0) {
    String command = Serial.readStringUntil('\n');
    if (command == "START_MONITORING_DISTANCE") {
            monitoringDistance = true;
            Serial.println("Distance monitoring started");
        } else if (command == "STOP_MONITORING_DISTANCE") {
            monitoringDistance = false;
            Serial.println("Distance monitoring stopped");
        } else if (command == "START_MONITORING_LIGHT") {
            monitoringLight = true;
            Serial.println("Light monitoring started");
        } else if (command == "STOP_MONITORING_LIGHT") {
            monitoringLight = false;
            Serial.println("Light monitoring stopped");
        } else {
            Serial.println("XNA");
    }
  }

  if (monitoringDistance ||monitoringLight ){
    int photoSensorValue = 0;
    int distance = 0;
    if (monitoringDistance) {
      digitalWrite(trigPin, LOW);
      delayMicroseconds(2);
      digitalWrite(trigPin, HIGH);
      delayMicroseconds(10);
      digitalWrite(trigPin, LOW);
      long duration = pulseIn(echoPin, HIGH);
      distance = duration * 0.034 / 2;
    } 
    if (monitoringLight) {
      photoSensorValue = analogRead(photoSensorPin);
    }

  StaticJsonDocument<200> jsonDoc;
  jsonDoc["photoSensor"] = photoSensorValue;
  jsonDoc["distance"] = distance;

  char jsonBuffer[512];
  serializeJson(jsonDoc, jsonBuffer);

  Serial.println(jsonBuffer);
  delay(420);
  }
}
